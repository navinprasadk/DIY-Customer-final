import React, { Component } from "react";
import { Header, Icon, Message, Grid } from "semantic-ui-react";
import Navbar from "./Header.jsx";
export default class Done extends Component {
  render() {
    return (
      <div className="done">
        <Navbar />
        <Grid style={{ margin: "0% 25%" }} centered columns={1}>
          <Grid.Column>
            <Message
              icon
              textAlign="center"
              style={{
                marginTop: "22%",
                fontFamily: "Poor Richard",
                fontSize: "20",
                color: "#FBE5D6",
                backgroundColor:'#333F50',
                border:"3px solid #FF7701"
              }}
            >
              <Icon name="shopping cart" />
              <Message.Content
                style={{
                  width: "100%",
                  fontFamily: "Poor Richard",
                  fontSize: "20"
                }}
              >
                <p>
                  Item successfully added to your{" "}
                  <b>{this.props.location.store}</b> store's cart
                </p>
                <p>
                  Go to your <b>{this.props.location.store}</b> store's cart and check
                  it out.
                </p>
              </Message.Content>
            </Message>
          </Grid.Column>
        </Grid>
        {/* <Header as='h2' icon textAlign='center' style={{marginTop:'20%'}}>
          <Icon name='settings' />
              Done
        </Header> */}
      </div>
    );
  }
}
