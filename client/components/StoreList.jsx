
// This file is not working, only for reference
// This file is not working, only for reference
// This file is not working, only for reference
// This file is not working, only for reference
// This file is not working, only for reference
// This file is not working, only for reference
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Redirect } from "react-router";
import $ from "jquery";
import {
  Dropdown,
  Menu,
  Label,
  Card,
  Form,
  Radio,
  Checkbox,
  Button,
  Input,
  Message,
  Segment
} from "semantic-ui-react";
import { CountryDropdown, RegionDropdown } from "react-country-region-selector";
import {Grid, Row, Col} from 'react-flexboxgrid';
import "../styles/StoreList.css";
import Header from "./Header.jsx";

const countryOptions = [
  {
    key: "United states",
    value: "United states",
    flag: "us",
    text: "United states"
  }
];
const stateOptions = [
  { key: "Alaska", value: "555555", text: "Alaska" },
  { key: "California", value: "888888", text: "California" },
  { key: "Florida", value: "999999", text: "Florida" },
  { key: "Georgia", value: "111111", text: "Georgia" },
  { key: "Hawaii", value: "222222", text: "Hawaii" },
  { key: "New Jersey", value: "333333", text: "New Jersey" },
  { key: "Ohio", value: "444444", text: "Ohio" },
  { key: "Texas", value: "555555", text: "Texas" },
  { key: "Washington", value: "666666", text: "Washington" },
  { key: "Virginia", value: "777777", text: "Virginia" }
];
const storeOptions = [
  { key: "John's Store", value: "John's Store", text: "John's Store" },
  {
    key: "Papa's John Store",
    value: "Papa's John Store",
    text: "Papa's John Store"
  }
];

export default class StoreList extends Component {
  constructor() {
    super();
    this.state = {
      storeStatus: true,
      buttonStatus: true,
      url: "",
      linkStatus: true,
      showMessage: false,
      proceedBtn: false,
      store: "",
      recommendedStore:false,
      nextButtonStatus:false,
      nextBtn:false,
      storeValue:""
    };
    this.handleStoreChange = this.handleStoreChange.bind(this);
    this.handleUrlChange = this.handleUrlChange.bind(this);
  }
  /*To enable store dropdown if state dropdown is selected*/
  regionChange() {
    this.setState({ storeStatus: false });
  }

  //To enable proceed button if store dropdown is selected
  storeChange(e, data) {
    this.setState({ linkStatus: false,buttonStatus: false });
  }


  handleUrlChange(e) {
    console.log("changedurl"+ e.target.value);
    this.setState({ /*buttonStatus: false,*/ url: e.target.value });
  }

  handleStoreChange(e,data){
    this.setState({nextButtonStatus:true,store: data.value})
  }

  handleProceed() {
    var that = this;
    $.ajax({
      type: "GET",
      url: "/checkUrl",
      data: { url:this.state.url },
      success: function(data) {
        console.log("data", data);
        if (data != "") {
          that.setState({ proceedBtn: true,recommendedStore:true });
        } else {
          console.log("no data");
          that.setState({ proceedBtn: false, showMessage: true });
        }
      },
      error: function(err) {
        that.setState({ buttonStatus: false, showMessage: true });
      }
    });
  }

  handleNext(){
      this.setState({nextBtn:true})
  }

  render() {
    return (
      <Grid>
      <div className="background-image">
      <div className="App">
        <Row top="xs">
          <Col xs={12} sm={12} md={12} lg={12}>
            <Header />
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={12} md={12} lg={12}>
            <Row end="xs">
              <Col xs={12} sm={12} md={6} lg={6}>
              <Card className="storeListCard" raised >
                <Card.Header
                  style={{
                    fontFamily: "Sitka Banner",
                    fontSize: "24",
                    marginTop: "8%",
                    fontWeight: "bold",
                    color:"#FBE5D6"
                  }}
                >
                  Fill Your Details
                </Card.Header>
                <Form>
                  <Row style={{ margin: "6%" }}>
                    <Col>
                      <Button
                        content="United states"
                        icon="globe"
                        labelPosition="left"
                        style={{
                          border: "2px solid white",
                          width: "90%",
                          fontFamily: "Sitka Banner",
                          fontSize: "18",
                          backgroundColor:"white"
                        }}
                      />
                    </Col>
                  </Row>
                  <Row style={{ margin: "6%" }}>
                    <Col>
                      {/* <Dropdown
                        button
                        className="icon "
                        floating
                        labeled
                        icon="circle outline"
                        options={stateOptions}
                        search
                        placeholder="Select a State"
                        style={{
                          border: "2px solid white",
                          width: "90%",
                          textAlign: "center",
                          fontFamily: "Sitka Banner",
                          fontSize: "18"
                        }}
                        onChange={this.regionChange.bind(this)}
                      /> */}
                      <Input
                        button
                        className="Icon urlInput"
                        labeled
                        className="icon "
                        icon="circle outline"
                        placeholder="Zip Code"
                        iconPosition="left"
                        style={{
                          border: "1px solid white",
                          borderRadius: "5%",
                          width: "90%",
                          textAlign: "center",
                          fontFamily: "Sitka Banner",
                          fontSize: "18"
                        }}
                        onChange={this.storeChange.bind(this)}
                      />
                    </Col>
                  </Row>
                  <Row style={{ margin: "6%" }}>
                    {/* <Grid.Column>
                      <Dropdown
                        button
                        className="icon "
                        floating
                        labeled
                        icon="shopping basket"
                        options={storeOptions}
                        search
                        placeholder="Select a Store"
                        style={{
                          border: "2px solid #CACBCD",
                          width: "90%",
                          textAlign: "center",
                          fontFamily: "Sitka Banner",
                          fontSize: "18"
                        }}
                        // disabled={this.state.storeStatus}
                        // onChange={this.storeChange.bind(this)}
                      />
                    </Grid.Column> */}
                  </Row>
                  <Row style={{ margin: "6%" }}>
                    <Col>
                      <Input
                        button
                        className="Icon urlInput"
                        labeled
                        className="icon "
                        icon="linkify"
                        placeholder="YouTube link"
                        iconPosition="left"
                        onChange={this.handleUrlChange}
                        disabled={this.state.linkStatus}
                        value = {this.state.url}
                        style={{
                          border: "1px solid black",
                          borderRadius: "5%",
                          width: "90%",
                          textAlign: "center",
                          fontFamily: "Sitka Banner",
                          fontSize: "18"
                        }}
                      />{" "}
                      {this.state.showMessage ? (
                        <Label basic color="red" pointing>
                          Please enter a valid URL
                        </Label>
                      ) : (
                        null
                      )}
                    </Col>
                  </Row>
                  {/* {this.state.showMessage?<Grid.Row style={{ margin: "10%" }}>
                    <Grid.Column>
                      <Message negative style={{padding:'4%'}}>
                        <Message.Header>Entered Invalid URL</Message.Header>
                      </Message>
                    </Grid.Column>
                  </Grid.Row>:''} */}
                  <Row style={{ margin: "10%" }}>
                    <Col>
                      {this.state.buttonStatus ? (
                        <Button  color="orange"
                          style={{
                            marginLeft: "40%",
                            fontFamily: "Sitka Banner",
                            fontSize: "18"
                          }}

                          type="submit"
                          disabled={this.state.buttonStatus}
                          onClick={this.handleProceed.bind(this)}
                        >
                          {" "}
                          Proceed
                        </Button>
                      ) : (
                        <Button color="orange"
                          style={{
                            marginLeft: "40%",
                            fontFamily: "Sitka Banner",
                            fontSize: "18"
                          }}
                          type="submit"
                          onClick={this.handleProceed.bind(this)}
                        >
                          {" "}
                          Proceed
                        </Button>
                      )}
                    </Col>
                  </Row>
                </Form>
                {this.state.nextBtn ? (
                  <Redirect
                    to={{
                      pathname: "/ingredients",
                      data: { url: this.state.url , store: this.state.store }
                    }}
                  />
                ) : (
                  null
                )}
              </Card>
              </Col>
            </Row>
          </Col>
        </Row>
        <br/>
        <Row>
          <Col xs={12} sm={12} md={12} lg={12}>
            <Row end="xs" middle="sm md lg">
              <Col xs={12} sm={12} md={6} lg={6}>
              {this.state.recommendedStore?
                <Segment raised style={{ backgroundColor: "#333f50", marginLeft:" 53%",marginRight:"3%",border:"4px solid #FF7701" }} >
            <Form >
            <Form.Field style={{
              fontWeight:"bolder",color:'#FBE5D6',fontSize:'20', fontFamily: "Sitka Banner"
            }}>
              Recommended store <b>{this.state.value}</b>
            </Form.Field>
            <Form.Field style={{fontWeight:"bolder",fontSize:'20px', fontFamily: "Sitka Banner"}}>
              <Radio
                style={{
                  fontWeight:"bolder",fontSize:'20px', fontFamily: "Sitka Banner", paddingRight:"5%"
                }}
                label='Wipro store'
                name='radioGroup'
                value='wipro'
                checked={this.state.storeValue === 'wipro'}
                onChange={this.handleStoreChange.bind(this)}
              />
            {/* </Form.Field>
            <Form.Field> */}
              <Radio style={{
                fontWeight:"bolder",fontSize:'20px', fontFamily: "Sitka Banner", paddingRight:"5%"
              }}
                label='EC store'
                name='radioGroup'
                value='ec'
                checked={this.state.storeValue === 'ec'}
                onChange={this.handleStoreChange.bind(this)}
              />
            {/* </Form.Field>
            <Form.Field> */}
              <Radio style={{
                fontWeight:"bolder",fontSize:'20px', fontFamily: "Sitka Banner", paddingRight:"5%"
              }}
                label='SJP store'
                name='radioGroup'
                value='sjp'
                checked={this.state.storeValue === 'sjp'}
                onChange={this.handleStoreChange.bind(this)}
              />
            {/* </Form.Field>
            <Form.Field> */}
              <Radio style={{
                fontWeight:"bolder", fontSize:'20px', fontFamily: "Sitka Banner"
              }}
                label='CDC store'
                name='radioGroup'
                value='cdc'
                checked={this.state.storeValue === 'cdc'}
                onChange={this.handleStoreChange.bind(this)}
              />
            </Form.Field>
          </Form>
          <br/>
          </Segment>

        :null}

        {this.state.nextButtonStatus?
            <Button color="orange"
                style={{
                    float:"right",margin:"-1% 3%",fontFamily: "Sitka Banner",
                  fontSize: "18"
                }}
                content='Next' icon='right arrow' labelPosition='right'
                type="submit"
                onClick = {this.handleNext.bind(this)}>
            </Button>
          :null}
              </Col>
            </Row>
          </Col>
        </Row>
        </div>
      </div>
      </Grid>

    );
  }
}
