import React from "react";
import ReactDOM from "react-dom";
import { Link, Redirect } from "react-router-dom";
import $ from "jquery";
import "../styles/StoreList.css";
import {
  List,
  Label,
  Tab,
  Table,
  Icon,
  Card,
  Modal,
  Image,
  Grid,
  Segment,
  Button,
  Header
} from "semantic-ui-react";
//import {Grid, Row, Col} from 'react-flexboxgrid';
import Navbar from "./Header.jsx";
import MainIngredientList from "./MainIngredientList.jsx";
import EssentialIngredientList from "./EssentialIngredient.jsx";
import EmptyMainIngredient from "./emptyMainIngredient.jsx";
import EmptyEssentialIngredient from "./emptyEssentialIngredient.jsx";
import { Scrollbars } from "react-custom-scrollbars";

class Ingredients extends React.Component {
  constructor() {
    super();
    this.state = {
      procedure: "",
      mainIngredient: "",
      essentialIngredient: "",
      height1: "",
      height2: "",
      displayEssential: false,
      displayMain: false,
      displayDoneBtn: false,
      title: "",
      modalOpen: false,
      successRedirect: false
    };
    this.handleDoneBtn = this.handleDoneBtn.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  componentDidMount() {
    this.setState({ height1: window.innerHeight - 400 + "px" });
    this.setState({ height2: window.innerHeight - 370 + "px" });
  }
  componentWillMount() {
    var that = this;
    console.log("url:" + that.props.location.data.url);
    $.ajax({
      type: "GET",
      url: "/ingredients",
      data: { url: that.props.location.data.url },
      success: function(data) {
        console.log(data);

        if (data[0].title != "") {
          that.setState({ title: data[0].title });
        } else {
          that.setState({ title: "" });
        }
        if (data[0].procedure != "") {
          that.setState({ procedure: data[0].procedure });
        } else {
          that.setState({ procedure: "No procedure" });
        }
        if (data[0].MainIngredient != "") {
          that.setState({ displayMain: true });
          var mainIngredient = data[0].MainIngredient;
          var mainIngredientList = mainIngredient.map((row, index) => {
            return (
              <MainIngredientList
                ingredients={row.name}
                quantity={row.quantity+" "+row.unit}
                key={index}
              />
            );
          });
        } else {
          that.setState({ displayMain: false });
        }
        if (data[0].EssentialIngredient != "") {
          that.setState({ displayEssential: true });
          var essentialIngredient = data[0].EssentialIngredient;
          var essentialIngredientList = essentialIngredient.map(
            (row, index) => {
              return (
                <EssentialIngredientList
                  essentialIngredient={row.name}
                  key={index}
                />
              );
            }
          );
        } else {
          that.setState({ displayEssential: false });
        }
        that.setState({
          mainIngredient: mainIngredientList,
          essentialIngredient: essentialIngredientList
        });
      },
      error: function(err) {
        alert("error");
      }
    });
  }

  handleDoneBtn() {
    this.setState({ displayDoneBtn: true });
  }

  handleOpen() {
    this.setState({ modalOpen: true });
  }

  handleClose() {
    this.setState({ modalOpen: false });
  }

  handleOk() {
    this.setState({ successRedirect: true });
  }

  render() {
    if (this.state.mainIngredient != "") {
      var dd = this.state.mainIngredient.map(item => {
        return (
          <Table.Row style={{ backgroundColor: "#333F50" }}>
            <Table.Cell style={{ fontFamily: "Poor Richard", fontSize: "20" }}>
              <Modal
                trigger={
                  <a className="anchor" onClick={this.handleDoneBtn}>
                    {item.props.ingredients}
                  </a>
                }
                closeIcon
              >
                <Modal.Content>
                  <p
                    className="mainIngredient"
                    style={{ fontFamily: "Poor Richard", fontSize: "20" }}
                  >
                    <Segment inverted style={{color:"#333750"}}>
                      <div>
                        <Header style={{textAlign:"center"}} inverted  size='huge'>  {item.props.ingredients}</Header>
                        <Image src={'../assets/images/Ingredients/'+item.props.ingredients+'.png'}></Image>
                      </div>
                    </Segment>

                  </p>
                </Modal.Content>
              </Modal>
            </Table.Cell>
            {((item.props.ingredients.length)>0)?<Table.Cell style={{ fontFamily: "Poor Richard", fontSize: "20" }}>
              {item.props.quantity}
            </Table.Cell>:null}
          </Table.Row>
        );
      });
    } else {
      var dd = (
        <div style={{ fontFamily: "Poor Richard", fontSize: "20" }}>
          It has no ingredients
        </div>
      );
    }

    if (this.state.essentialIngredient != "") {
      var essential = this.state.essentialIngredient.map(item => {
        return (
          <Table.Row style={{ backgroundColor: "#333F50" }}>
            <Table.Cell style={{ fontFamily: "Poor Richard", fontSize: "20" }}>
              <Modal
                trigger={
                  <a className="anchor" onClick={this.handleDoneBtn}>
                    {item.props.essentialIngredient}
                  </a>
                }
                closeIcon
              >
                <Modal.Content>
                  <p
                    className="mainIngredient"
                    style={{ fontFamily: "Poor Richard", fontSize: "20" }}
                  >
                    <Segment inverted style={{color:"#333750"}}>
                      <div>
                        <Header style={{textAlign:"center"}} inverted  size='huge'>  {item.props.essentialIngredient}</Header>
                        <Image src={'../assets/images/Ingredients/'+item.props.essentialIngredient+'.png'}></Image>
                      </div>
                    </Segment>

                  </p>
                </Modal.Content>
              </Modal>
            </Table.Cell>
          </Table.Row>
        );
      });
    }
    const panes = [
      {
        menuItem: "MAIN INGREDIENTS",
        pane: (
          <Tab.Pane
            key="tab1"
            style={{
              backgroundColor: "#333F50",
              fontFamily: "Poor Richard",
              border: "3px solid #FF7701"
            }}
          >
            {/* <p>This tab has ingredients which you can add to cart.</p> */}
            {this.state.displayMain ? (
              <Scrollbars style={{ height: this.state.height1 }}>
                <Table color="#333F50" inverted padded>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell
                        style={{
                          backgroundColor: "#333F50",
                          fontFamily: "Poor Richard",
                          fontSize: "20",
                          color: "#FBE5D6"
                        }}
                      >
                        Ingredients
                      </Table.HeaderCell>
                      <Table.HeaderCell
                        style={{
                          backgroundColor: "#333F50",
                          fontFamily: "Poor Richard",
                          fontSize: "20",
                          color: "#FBE5D6"
                        }}
                      >
                        Quantity
                      </Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body
                    style={{
                      fontFamily: "Poor Richard",
                      fontSize: "20",
                      color: "#FBE5D6"
                    }}
                  >
                    {dd}
                  </Table.Body>
                </Table>
              </Scrollbars>
            ) : (
              <EmptyMainIngredient />
            )}
          </Tab.Pane>
        )
      },
      {
        menuItem: "SUPPLEMENTARY",
        pane: (
          <Tab.Pane
            key="tab2"
            style={{
              backgroundColor: "#333F50",
              fontFamily: "Poor Richard",
              border: "3px solid #FF7701"
            }}
          >
            {/* <p>This tab has a additional ingredients which you can add to cart.</p> */}
            {this.state.displayEssential ? (
              <Scrollbars style={{ height: this.state.height1 }}>
                <Table color="#333F50" inverted padded>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell
                        style={{
                          backgroundColor: "#333F50",
                          fontFamily: "Poor Richard",
                          fontSize: "20",
                          color: "#FBE5D6"
                        }}
                      >
                        Item
                      </Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body
                    style={{
                      fontFamily: "Poor Richard",
                      fontSize: "20",
                      color: "#FBE5D6"
                    }}
                  >
                    {essential}
                  </Table.Body>
                </Table>
              </Scrollbars>
            ) : (
              <EmptyEssentialIngredient />
            )}
          </Tab.Pane>
        )
      }
    ];

    return (
      <div className="Ingredients">
          <Navbar />
          <div>
            {/* <Tab  panes={panes} renderActiveOnly={false} />; */}
            <Header
              as="h2"
              style={{
                color: "white",
                textAlign: "center",
                textTransform: "uppercase",
                fontFamily: "Sitka Banner",
                fontSize: "24",
                color: "#333f50"
              }}
            >
              {" "}
              {this.state.title}{" "}
            </Header>
          <Grid columns={2}>
            <Grid.Column className="ingredientsGrid">
              <Tab
                // menu={{ color:"black", inverted: true}}
                panes={panes}
                renderActiveOnly={false}
                style={{
                  fontFamily: "Poor Richard",
                  fontSize: "20",
                  textAlign: "justify"
                }}
              />
            </Grid.Column>
            <Grid.Column className="procedureGrid">
              <Header
                as="h3"
                title="procedure"
                style={{
                  marginTop: "3%",
                  fontFamily: "Poor Richard",
                  fontSize: "20",
                  color: "#333f50"
                }}
                inverted
              >
                PROCEDURE
              </Header>
              {/* <Label as='a' color='black' style={{marginLeft:'3%'}} ribbon>Procedure</Label> */}
              {/*}  <Segment><Header as='h3'  content='Procedure' icon='bars'/></Segment>*/}
              <Scrollbars
                style={{
                  height: this.state.height2,
                  border: "3px solid #FF7701"
                }}
              >
                <Segment raised style={{ backgroundColor: "#333f50" }}>
                  <p
                    style={{
                      textAlign: "justify",
                      padding: "2%",
                      fontFamily: "Poor Richard",
                      fontSize: "20",
                      color: "#FBE5D6"
                    }}
                  >
                    {this.state.procedure}
                  </p>
                </Segment>
              </Scrollbars>
            </Grid.Column>
          </Grid>
          <br/>
                <Segment
                  style={{
                    fontFamily: "Poor Richard",
                    fontSize: "20",
                    textAlign: "justify",
                    backgroundColor: "#333F50",
                    color: "#FBE5D6",
                    border: "3px solid #FF7701",
                    margin:"0% 2%"

                  }}
                >
                  The above list shows the quantity according to the suggestion by
                  the blogger. Kindly edit the quantity in retailers site
                  according to the need.
                </Segment>
                <br/>
            <Grid>
            <Modal
              trigger={
                this.state.displayDoneBtn ? (
                  <Button
                    color="orange"
                    style={{
                      marginLeft: "3%",
                      fontFamily: "Sitka Banner",
                      fontSize: "18",
                      width: "8%"
                    }}
                    onClick={this.handleOpen}
                  >
                    Done
                  </Button>
                ) : (
                  ""
                )
              }
              open={this.state.modalOpen}
              onClose={this.handleClose}
              basic
              size="small"
            >
              <Header
                icon="browser"
                style={{ color: "#FBE5D6" }}
                content="Confirmation"
              />
              <Modal.Content style={{ color: "#FBE5D6" }}>
                <h3>Are you sure you want to continue?</h3>
              </Modal.Content>
              <Modal.Actions>
                <Button color="green" onClick={this.handleOk} inverted>
                  <Icon name="checkmark" /> Ok
                </Button>
                <Button color="red" onClick={this.handleClose} inverted>
                  <Icon name="remove" /> Cancel
                </Button>
              </Modal.Actions>
              {this.state.successRedirect ? (
                <Redirect
                  color="orange"
                  to={{
                    pathname: "/done",
                    store: this.props.location.data.store
                  }}
                />
              ) : (
                ""
              )}
            </Modal>
            </Grid>
            </div>
            </div>
    );
  }
}

export default Ingredients;
