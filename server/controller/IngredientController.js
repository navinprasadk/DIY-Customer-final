let ingredientSchema = require("./../models/IngredientSchema.js");
let storeSchema = require("./../models/storeSchema.js");

module.exports = {
  checkUrl: function(req, res) {
    var url = req.query.url;
    console.log("url:" + url);
    ingredientSchema.find({ url: url }, function(err, details) {
      if (err) {
        res.send(err);
      } else {
        //console.log(details);
        res.send(details);
      }
    });
  },
  getIngredientDetails: function(req, res) {
    var url = req.query.url;
    console.log("getIngredientsurl:" + url);
    ingredientSchema.find({ url: url }, function(err, details) {
      if (err) {
        res.send(err);
      } else {
        //console.log(details);
        res.send(details);
      }
    });
  },
  getStoreNames: function(req, res) {
    var zipcode = req.query.zipcode;
    console.log("zipcode:" + zipcode);
    storeSchema.find({ zipcode: zipcode }, function(err, stores) {
      if (err) {
        res.send(err);
      } else {
        console.log(stores);
        res.send(stores);
      }
    });
  }
};
