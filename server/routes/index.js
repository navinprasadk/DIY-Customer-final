var express = require("express");
var router = express.Router();
let ingredientController = require("../controller/IngredientController");

router.get("/ingredients", ingredientController.getIngredientDetails);
router.get("/checkUrl", ingredientController.checkUrl);
router.get("/stores", ingredientController.getStoreNames);


module.exports = router;
